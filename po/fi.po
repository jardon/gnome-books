# Finnish translation for gnome-documents.
# Copyright (C) 2012 gnome-documents's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-documents package.
#
# Gnome 2012-03 Finnish translation sprint participants:
# Niklas Laxström
# Jiri Grönroos <jiri.gronroos+l10n@iki.fi>, 2012, 2013, 2014, 2015, 2016, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-documents master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-books/issues\n"
"POT-Creation-Date: 2019-08-02 16:26+0000\n"
"PO-Revision-Date: 2019-08-17 16:26+0300\n"
"Last-Translator: Jiri Grönroos <jiri.gronroos+l10n@iki.fi>\n"
"Language-Team: suomi <lokalisointi-lista@googlegroups.com>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.0.6\n"
"X-POT-Import-Date: 2012-03-05 14:46:33+0000\n"

#: data/org.gnome.Books.appdata.xml.in:7 data/org.gnome.Books.desktop.in:3
#: src/application.js:104 src/overview.js:1038
msgid "Books"
msgstr "Kirjat"

#: data/org.gnome.Books.appdata.xml.in:8
msgid "An e-book manager application for GNOME"
msgstr "Kirjojen hallintasovellus Gnomelle"

#: data/org.gnome.Books.appdata.xml.in:10
msgid ""
"A simple application to access and organize your e-books on GNOME. It is "
"meant to be a simple and elegant replacement for using a file manager to "
"deal with e-books."
msgstr ""
"Yksinkertainen Gnome-sovellus kirjojen käyttöä, organisointia ja jakamista "
"varten. Sen on tarkoitus korvata tiedostonhallinta kirjoihin liittyvässä "
"käytössä. Saumattoman pilvi-integraation tarjoaa Gnomen verkkotilit."

#: data/org.gnome.Books.appdata.xml.in:15
msgid "It lets you:"
msgstr "Sen avulla voit:"

#: data/org.gnome.Books.appdata.xml.in:17
msgid "View recent e-books"
msgstr "Katsella viimeisimpiä kirjoja"

#: data/org.gnome.Books.appdata.xml.in:18
msgid "Search through e-books"
msgstr "Etsiä kirjojen sisällöstä"

#: data/org.gnome.Books.appdata.xml.in:19
msgid "View e-books (ePubs and comics) fullscreen"
msgstr "Katsele sähköisiä kirjoja (ePubeja ja sarjakuvia) koko näytön tilassa"

#: data/org.gnome.Books.appdata.xml.in:31
msgid "The GNOME Project"
msgstr "Gnome-projekti"

#: data/org.gnome.Books.desktop.in:4
msgid "Access, manage and share books"
msgstr "Käytä, hallitse ja jaa kirjoja"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Books.desktop.in:15
msgid "Books;Comics;ePub;PDF;"
msgstr ""
"Books;Comics;ePub;PDF;Kirjat;Sarjakuvat;Sarjikset;Dokumentit;Asiakirjat;"
"Sähkökirjat;"

#: data/org.gnome.books.gschema.xml:5
msgid "View as"
msgstr "Näkymä"

#: data/org.gnome.books.gschema.xml:6
msgid "View as type"
msgstr "Näkymän tyyppi"

#: data/org.gnome.books.gschema.xml:10
msgid "Sort by"
msgstr "Järjestä"

#: data/org.gnome.books.gschema.xml:11
msgid "Sort by type"
msgstr "Järjestä tyypin mukaan"

#: data/org.gnome.books.gschema.xml:15
msgid "Window size"
msgstr "Ikkunan koko"

#: data/org.gnome.books.gschema.xml:16
msgid "Window size (width and height)."
msgstr "Ikkunan koko (leveys ja korkeus)."

#: data/org.gnome.books.gschema.xml:20
msgid "Window position"
msgstr "Ikkunan sijainti"

#: data/org.gnome.books.gschema.xml:21
msgid "Window position (x and y)."
msgstr "Ikkunan sijainti (x ja y)."

#: data/org.gnome.books.gschema.xml:25
msgid "Window maximized"
msgstr "Ikkuna suurennettu"

#: data/org.gnome.books.gschema.xml:26
msgid "Window maximized state"
msgstr "Ikkunan suurennettu tila"

#: data/org.gnome.books.gschema.xml:30
msgid "Night mode"
msgstr "Yötila"

#: data/org.gnome.books.gschema.xml:31
msgid "Whether the application is in night mode."
msgstr "Toimiiko sovellus yötilassa."

#: data/ui/books-app-menu.ui:6 src/preview.js:452
msgid "Night Mode"
msgstr "Yötila"

#: data/ui/books-app-menu.ui:12
msgid "Keyboard Shortcuts"
msgstr "Pikanäppäimet"

#: data/ui/books-app-menu.ui:16
msgid "Help"
msgstr "Ohje"

#: data/ui/books-app-menu.ui:20
msgid "About Books"
msgstr "Tietoja - Kirjat"

#: data/ui/help-overlay.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "Yleiset"

#: data/ui/help-overlay.ui:15
msgctxt "shortcut window"
msgid "Show help"
msgstr "Näytä ohje"

#: data/ui/help-overlay.ui:22
msgctxt "shortcut window"
msgid "Quit"
msgstr "Lopeta"

#: data/ui/help-overlay.ui:29
msgctxt "shortcut window"
msgid "Search"
msgstr "Etsi"

#: data/ui/help-overlay.ui:36
msgctxt "shortcut window"
msgid "Print the current document"
msgstr "Tulosta nykyinen asiakirja"

#: data/ui/help-overlay.ui:45
msgctxt "shortcut window"
msgid "Navigation"
msgstr "Liikkuminen"

#: data/ui/help-overlay.ui:49 data/ui/help-overlay.ui:57
msgctxt "shortcut window"
msgid "Go back"
msgstr "Siirry takaisin"

#: data/ui/help-overlay.ui:67
msgctxt "shortcut window"
msgid "Preview"
msgstr "Esikatselu"

#: data/ui/help-overlay.ui:71
msgctxt "shortcut window"
msgid "Zoom in"
msgstr "Lähennä"

#: data/ui/help-overlay.ui:78
msgctxt "shortcut window"
msgid "Zoom out"
msgstr "Loitonna"

#: data/ui/help-overlay.ui:85
msgctxt "shortcut window"
msgid "Bookmark the current page"
msgstr "Lisää sivu kirjanmerkkeihin"

#: data/ui/help-overlay.ui:92
msgctxt "shortcut window"
msgid "Open places and bookmarks dialog"
msgstr "Avaa sijainti- ja kirjanmerkki-ikkuna"

#: data/ui/help-overlay.ui:99
msgctxt "shortcut window"
msgid "Copy selected text to clipboard"
msgstr "Kopioi valittu teksti leikepöydälle"

#: data/ui/help-overlay.ui:106
msgctxt "shortcut window"
msgid "Rotate counterclockwise"
msgstr "Kierrä vastapäivään"

#: data/ui/help-overlay.ui:113
msgctxt "shortcut window"
msgid "Rotate clockwise"
msgstr "Kierrä myötäpäivään"

#: data/ui/help-overlay.ui:120
msgctxt "shortcut window"
msgid "Next occurrence of the search string"
msgstr "Hakuehdon seuraava esiintymä"

#: data/ui/help-overlay.ui:127
msgctxt "shortcut window"
msgid "Previous occurrence of the search string"
msgstr "Hakuehdon edellinen esiintymä"

#: data/ui/help-overlay.ui:134
msgctxt "shortcut window"
msgid "Presentation mode"
msgstr "Esitystila"

#: data/ui/help-overlay.ui:141
msgctxt "shortcut window"
msgid "Open action menu"
msgstr "Avaa toimintovalikko"

#: data/ui/help-overlay.ui:148
msgctxt "shortcut window"
msgid "Fullscreen"
msgstr "Koko näyttö"

#: data/ui/organize-collection-dialog.ui:40
msgid "Enter a name for your first collection"
msgstr "Anna nimi ensimmäiselle kokoelmallesi"

#: data/ui/organize-collection-dialog.ui:56
#: data/ui/organize-collection-dialog.ui:113
msgid "New Collection…"
msgstr "Uusi kokoelma…"

#: data/ui/organize-collection-dialog.ui:73
#: data/ui/organize-collection-dialog.ui:122
msgid "Add"
msgstr "Lisää"

#: data/ui/organize-collection-dialog.ui:163 data/ui/selection-toolbar.ui:88
#: src/overview.js:1042 src/search.js:121
msgid "Collections"
msgstr "Kokoelmat"

#: data/ui/organize-collection-dialog.ui:167 src/overview.js:598
msgid "Cancel"
msgstr "Peru"

#: data/ui/organize-collection-dialog.ui:173
msgid "Done"
msgstr "Valmis"

#: data/ui/preview-context-menu.ui:6
msgid "_Copy"
msgstr "_Kopioi"

#. Translators: this is the Open action in a context menu
#: data/ui/preview-menu.ui:6 data/ui/selection-toolbar.ui:9
#: src/selections.js:976
msgid "Open"
msgstr "Avaa"

#: data/ui/preview-menu.ui:10
msgid "Edit"
msgstr "Muokkaa"

#: data/ui/preview-menu.ui:15
msgid "Print…"
msgstr "Tulosta…"

#: data/ui/preview-menu.ui:21 src/preview.js:461
msgid "Fullscreen"
msgstr "Koko näyttö"

#: data/ui/preview-menu.ui:27
msgid "Present"
msgstr "Esitys"

#: data/ui/preview-menu.ui:35
msgid "Zoom In"
msgstr "Lähennä"

#: data/ui/preview-menu.ui:41
msgid "Zoom Out"
msgstr "Loitonna"

#: data/ui/preview-menu.ui:49
msgid "Rotate ↶"
msgstr "Käännä ↶"

#: data/ui/preview-menu.ui:55
msgid "Rotate ↷"
msgstr "Käännä ↷"

#: data/ui/preview-menu.ui:62 data/ui/selection-toolbar.ui:79
#: src/properties.js:61
msgid "Properties"
msgstr "Ominaisuudet"

#: data/ui/selection-menu.ui:6
msgid "Select All"
msgstr "Valitse kaikki"

#: data/ui/selection-menu.ui:11
msgid "Select None"
msgstr "Poista kaikki valinnat"

#: data/ui/view-menu.ui:23
msgid "View items as a grid of icons"
msgstr "Näytä kohteet kuvakeruudukkona"

#: data/ui/view-menu.ui:40
msgid "View items as a list"
msgstr "Näytä kohteet luettelona"

#: data/ui/view-menu.ui:73
msgid "Sort"
msgstr "Järjestä"

#: data/ui/view-menu.ui:84
msgid "Author"
msgstr "Tekijä"

#: data/ui/view-menu.ui:93
msgid "Date"
msgstr "Päiväys"

#: data/ui/view-menu.ui:102
msgid "Name"
msgstr "Nimi"

#: src/application.js:113
msgid "Show the version of the program"
msgstr "Näytä ohjelman versio"

#: src/documents.js:779
msgid "Failed to print document"
msgstr "Asiakirjan tulostaminen epäonnistui"

#. Translators: this refers to local documents
#: src/documents.js:821 src/search.js:405
msgid "Local"
msgstr "Paikallinen"

#: src/documents.js:878
msgid "Collection"
msgstr "Kokoelma"

#: src/documents.js:1072
msgid ""
"You are using a preview of Books. Full viewing capabilities are coming soon!"
msgstr ""
"Käytössäsi on Kirjat-sovelluksen esiversio. Parannetut katseluominaisuudet "
"tulevat pian!"

#. Translators: %s is the title of a document
#: src/documents.js:1098
#, javascript-format
msgid "Oops! Unable to load “%s”"
msgstr "Hups! Kohteen “%s” lataus epäonnistui"

#: src/epubview.js:245
#, javascript-format
msgid "chapter %s of %s"
msgstr "kappale %s/%s"

#: src/evinceview.js:521 src/lib/gd-places-bookmarks.c:656
msgid "Bookmarks"
msgstr "Kirjanmerkit"

#: src/evinceview.js:529
msgid "Bookmark this page"
msgstr "Lisää sivu kirjanmerkkeihin"

#: src/lib/gd-nav-bar.c:242
#, c-format
msgid "Page %u of %u"
msgstr "Sivu %u/%u"

#: src/lib/gd-pdf-loader.c:142
msgid "Unable to load the document"
msgstr "Asiakirjan lataus epäonnistui"

#. Translators: %s is the number of the page, already formatted
#. * as a string, for example "Page 5".
#.
#: src/lib/gd-places-bookmarks.c:321
#, c-format
msgid "Page %s"
msgstr "Sivu %s"

#: src/lib/gd-places-bookmarks.c:384
msgid "No bookmarks"
msgstr "Ei kirjanmerkkejä"

#: src/lib/gd-places-bookmarks.c:392 src/lib/gd-places-links.c:257
msgid "Loading…"
msgstr "Ladataan…"

#: src/lib/gd-places-links.c:342
msgid "No table of contents"
msgstr "Ei sisällysluetteloa"

#: src/lib/gd-places-links.c:514
msgid "Contents"
msgstr "Sisältö"

#: src/lib/gd-utils.c:318
msgid "translator-credits"
msgstr "Jiri Grönroos, 2012-2019"

#: src/lib/gd-utils.c:319
msgid "An e-books manager application"
msgstr "Kirjojen hallintasovellus"

#: src/mainToolbar.js:95
msgctxt "menu button tooltip"
msgid "Menu"
msgstr "Valikko"

#: src/mainToolbar.js:106
msgctxt "toolbar button tooltip"
msgid "Search"
msgstr "Etsi"

#: src/mainToolbar.js:116
msgid "Back"
msgstr "Takaisin"

#. Translators: only one item has been deleted and %s is its name
#: src/notifications.js:48
#, javascript-format
msgid "“%s” deleted"
msgstr "“%s” poistettu"

#. Translators: one or more items might have been deleted, and %d
#. is the count
#: src/notifications.js:52
#, javascript-format
msgid "%d item deleted"
msgid_plural "%d items deleted"
msgstr[0] "%d kohde poistettu"
msgstr[1] "%d kohdetta poistettu"

#: src/notifications.js:61 src/selections.js:383
msgid "Undo"
msgstr "Kumoa"

#: src/notifications.js:159
#, javascript-format
msgid "Printing “%s”: %s"
msgstr "Tulostetaan “%s”: %s"

#: src/notifications.js:210
msgid "Your documents are being indexed"
msgstr "Asiakirjojasi indeksoidaan"

#: src/notifications.js:211
msgid "Some documents might not be available during this process"
msgstr ""
"Jotkin asiakirjat eivät välttämättä ole käytettävissä tämän toimenpiteen "
"aikana"

#: src/overview.js:287
msgid "No collections found"
msgstr "Kokoelmia ei löytynyt"

#: src/overview.js:289
msgid "No books found"
msgstr "Kirjoja ei löytynyt"

#: src/overview.js:301
msgid "Try a different search"
msgstr "Kokeile eri hakuehtoja"

#: src/overview.js:303
msgid "You can create collections from the Books view"
msgstr "Voit luoda kokoelmia kirjanäkymästä"

#. translators: %s is the location of the Documents folder.
#: src/overview.js:311
#, javascript-format
msgid "Books from your <a href=\"%s\">Documents</a> folder will appear here"
msgstr "Kirjat <a href=\"%s\">Asiakirjat</a>-kansiosta ilmestyvät tänne"

#. Translators: this is the menu to change view settings
#: src/overview.js:544
msgid "View Menu"
msgstr "Näkymävalikko"

#: src/overview.js:572
msgid "Click on items to select them"
msgstr "Napsauta kohteita valitaksesi ne"

#: src/overview.js:574
#, javascript-format
msgid "%d selected"
msgid_plural "%d selected"
msgstr[0] "%d valittu"
msgstr[1] "%d valittu"

#: src/overview.js:655
msgid "Select Items"
msgstr "Valitse kohteet"

#: src/overview.js:862
msgid "Yesterday"
msgstr "Eilen"

#: src/overview.js:864
#, javascript-format
msgid "%d day ago"
msgid_plural "%d days ago"
msgstr[0] "%d päivä sitten"
msgstr[1] "%d päivää sitten"

#: src/overview.js:868
msgid "Last week"
msgstr "Viime viikolla"

#: src/overview.js:870
#, javascript-format
msgid "%d week ago"
msgid_plural "%d weeks ago"
msgstr[0] "%d viikko sitten"
msgstr[1] "%d viikkoa sitten"

#: src/overview.js:874
msgid "Last month"
msgstr "Viime kuussa"

#: src/overview.js:876
#, javascript-format
msgid "%d month ago"
msgid_plural "%d months ago"
msgstr[0] "%d kuukausi sitten"
msgstr[1] "%d kuukautta sitten"

#: src/overview.js:880
msgid "Last year"
msgstr "Viime vuonna"

#: src/overview.js:882
#, javascript-format
msgid "%d year ago"
msgid_plural "%d years ago"
msgstr[0] "%d vuosi sitten"
msgstr[1] "%d vuotta sitten"

#: src/password.js:42
msgid "Password Required"
msgstr "Salasana vaaditaan"

#: src/password.js:45
msgid "_Unlock"
msgstr "_Avaa lukitus"

#: src/password.js:61
#, javascript-format
msgid "Document %s is locked and requires a password to be opened."
msgstr "Asiakirja %s on lukittu ja se vaatii salasanan avausta varten."

#: src/password.js:75
msgid "_Password"
msgstr "_Salasana"

#. Translators: this is the Open action in a context menu
#: src/preview.js:444 src/selections.js:973
#, javascript-format
msgid "Open with %s"
msgstr "Avaa sovelluksella %s"

#: src/preview.js:770
msgid "Find Previous"
msgstr "Etsi edellinen"

#: src/preview.js:776
msgid "Find Next"
msgstr "Etsi seuraava"

#. Title item
#. Translators: "Title" is the label next to the document title
#. in the properties dialog
#: src/properties.js:81
msgctxt "Document Title"
msgid "Title"
msgstr "Otsikko"

#. Translators: "Author" is the label next to the document author
#. in the properties dialog
#: src/properties.js:90
msgctxt "Document Author"
msgid "Author"
msgstr "Tekijä"

#. Source item
#: src/properties.js:97
msgid "Source"
msgstr "Lähde"

#. Date Modified item
#: src/properties.js:103
msgid "Date Modified"
msgstr "Muokkauspäivä"

#: src/properties.js:110
msgid "Date Created"
msgstr "Luontipäivä"

#. Document type item
#. Translators: "Type" is the label next to the document type
#. (PDF, spreadsheet, ...) in the properties dialog
#: src/properties.js:119
msgctxt "Document Type"
msgid "Type"
msgstr "Tyyppi"

#. Translators: "Type" refers to a search filter on the document type
#. (e-Books, Comics, ...)
#: src/search.js:116
msgctxt "Search Filter"
msgid "Type"
msgstr "Tyyppi"

#. Translators: this refers to documents
#: src/search.js:119 src/search.js:225 src/search.js:399
msgid "All"
msgstr "Kaikki"

#: src/search.js:127
msgid "e-Books"
msgstr "eKirjat"

#: src/search.js:131
msgid "Comics"
msgstr "Sarjakuvat"

#. Translators: this is a verb that refers to "All", "Title", "Author",
#. and "Content" as in "Match All", "Match Title", "Match Author", and
#. "Match Content"
#: src/search.js:222
msgid "Match"
msgstr "Vastaavuus"

#. Translators: "Title" refers to "Match Title" when searching
#: src/search.js:228
msgctxt "Search Filter"
msgid "Title"
msgstr "Otsikko"

#. Translators: "Author" refers to "Match Author" when searching
#: src/search.js:231
msgctxt "Search Filter"
msgid "Author"
msgstr "Tekijä"

#. Translators: "Content" refers to "Match Content" when searching
#: src/search.js:234
msgctxt "Search Filter"
msgid "Content"
msgstr "Sisältö"

#: src/search.js:395
msgid "Sources"
msgstr "Lähteet"

#: src/selections.js:356 src/selections.js:358
msgid "Rename…"
msgstr "Nimeä uudelleen…"

#: src/selections.js:362 src/selections.js:364
msgid "Delete"
msgstr "Poista"

#: src/selections.js:378
#, javascript-format
msgid "“%s” removed"
msgstr "“%s” poistettu"

#: src/selections.js:775
msgid "Rename"
msgstr "Nimeä uudelleen"

#. Translators: "Collections" refers to documents in this context
#: src/selections.js:781
msgctxt "Dialog Title"
msgid "Collections"
msgstr "Kokoelmat"

#: src/trackerController.js:176
msgid "Unable to fetch the list of documents"
msgstr "Asiakirjalistauksen nouto epäonnistui"

#~ msgid "org.gnome.Books"
#~ msgstr "org.gnome.Books"

#~ msgid "About Documents"
#~ msgstr "Tietoja - Asiakirjat"

#~ msgid "Print e-books"
#~ msgstr "Tulostaa kirjoja"

#~ msgid "Documents"
#~ msgstr "Asiakirjat"

#~ msgid "A document manager application for GNOME"
#~ msgstr "Asiakirjojen hallintasovellus Gnomelle"

#~ msgid ""
#~ "A simple application to access, organize and share your documents on "
#~ "GNOME. It is meant to be a simple and elegant replacement for using a "
#~ "file manager to deal with documents. Seamless cloud integration is "
#~ "offered through GNOME Online Accounts."
#~ msgstr ""
#~ "Yksinkertainen Gnome-sovellus asiakirjojen käyttöä, organisointia ja "
#~ "jakamista varten. Sen on tarkoitus korvata tiedostonhallinta "
#~ "asiakirjoihin liittyvässä työskentelyssä. Saumattoman pilvi-integraation "
#~ "tarjoaa Gnomen verkkotilit."

#~ msgid "View recent local and online documents"
#~ msgstr "Katsella paikallisia ja verkossa olevia asiakirjoja"

#~ msgid "Access your Google, ownCloud or OneDrive content"
#~ msgstr "Käyttää Google-, ownCloud- ja OneDrive-tiliesi sisältöä"

#~ msgid "Search through documents"
#~ msgstr "Etsiä asiakirjojen sisällöstä"

#~ msgid "See new documents shared by friends"
#~ msgstr "Tarkastella kaverien jakamia asiakirjoja"

#~ msgid "View documents fullscreen"
#~ msgstr "Katsella asiakirjoja koko näytön tilassa"

#~ msgid "Print documents"
#~ msgstr "Tulostaa asiakirjoja"

#~ msgid "Select favorites"
#~ msgstr "Valita suosikkeja"

#~ msgid "Allow opening full featured editor for non-trivial changes"
#~ msgstr "Avata erillisen muokkaimen suuria muutoksia varten"

#~ msgid "Access, manage and share documents"
#~ msgstr "Käytä, hallitse ja jaa asiakirjoja"

#~ msgid "org.gnome.Documents"
#~ msgstr "org.gnome.Documents"

#~ msgid "Docs;PDF;Document;"
#~ msgstr "Asiakirjat;PDF;asiakirja;dokumentti;dokumentit;Docs;Document;"

#~ msgid "About"
#~ msgstr "Tietoja"

#~ msgctxt "app menu"
#~ msgid "Quit"
#~ msgstr "Lopeta"

#~ msgid "GNOME"
#~ msgstr "Gnome"

#~ msgid "Getting Started with Documents"
#~ msgstr "Asiakirjat ja käytön aloitus"

#~ msgid "Google Docs"
#~ msgstr "Google Docs"

#~ msgid "Google"
#~ msgstr "Google"

#~ msgid "Spreadsheet"
#~ msgstr "Taulukko"

#~ msgid "Presentation"
#~ msgstr "Esitys"

#~ msgid "e-Book"
#~ msgstr "Kirja"

#~ msgid "Document"
#~ msgstr "Asiakirja"

#~ msgid "ownCloud"
#~ msgstr "ownCloud"

#~ msgid "OneDrive"
#~ msgstr "OneDrive"

#~ msgid "Please check the network connection."
#~ msgstr "Tarkista verkkoyhteys."

#~ msgid "Please check the network proxy settings."
#~ msgstr "Tarkista verkkoyhteyden välityspalvelimen asetukset."

#~ msgid "Unable to sign in to the document service."
#~ msgstr "Asiakirjapalveluun kirjautuminen epäonnistui."

#~ msgid "Unable to locate this document."
#~ msgstr "Tämän asiakirjan paikantaminen epäonnistui."

#~ msgid "Hmm, something is fishy (%d)."
#~ msgstr "Hmm, jokin meni pieleen (%d)."

#~ msgid ""
#~ "LibreOffice support is not available. Please contact your system "
#~ "administrator."
#~ msgstr ""
#~ "LibreOffice-tuki ei ole käytettävissä. Ota yhteys järjestelmän "
#~ "ylläpitäjään."

#~ msgid "View"
#~ msgstr "Näkymä"

#~ msgid "A document manager application"
#~ msgstr "Asiakirjojen hallintasovellus"

#~ msgid "Fetching documents from %s"
#~ msgstr "Noudetaan asiakirjaa kohteesta %s"

#~ msgid "Fetching documents from online accounts"
#~ msgstr "Noudetaan asiakirjoja verkkotileiltä"

#~ msgid "No documents found"
#~ msgstr "Asiakirjoja ei löytynyt"

#~ msgid "You can create collections from the Documents view"
#~ msgstr "Voit luoda kokoelmia asiakirjanäkymästä"

#~ msgid "Running in presentation mode"
#~ msgstr "Toimitaan esitystilassa"

#~ msgid "Present On"
#~ msgstr "Esitys näytöllä"

#~ msgid "Mirrored"
#~ msgstr "Peilattu"

#~ msgid "Primary"
#~ msgstr "Ensisijainen"

#~ msgid "Off"
#~ msgstr "Pois"

#~ msgid "Secondary"
#~ msgstr "Toissijainen"

#~ msgid "PDF Documents"
#~ msgstr "PDF-asiakirjat"

#~ msgid "Presentations"
#~ msgstr "Esitykset"

#~ msgid "Spreadsheets"
#~ msgstr "Taulukot"

#~ msgid "Text Documents"
#~ msgstr "Tekstiasiakirjat"

#~ msgid "%s (%s)"
#~ msgstr "%s (%s)"

#~ msgid "Sharing Settings"
#~ msgstr "Jakamisen asetukset"

#~ msgid "Document permissions"
#~ msgstr "Asiakirjan käyttöoikeudet"

#~ msgid "Change"
#~ msgstr "Muuta"

#~ msgid "Private"
#~ msgstr "Yksityinen"

#~ msgid "Public"
#~ msgstr "Julkinen"

#~ msgid "Everyone can edit"
#~ msgstr "Kaikki voivat muokata"

#~ msgid "Add people"
#~ msgstr "Lisää henkilöitä"

#~ msgid "Enter an email address"
#~ msgstr "Anna sähköpostiosoite"

#~ msgid "Can edit"
#~ msgstr "Voi muokata"

#~ msgid "Can view"
#~ msgstr "Voi katsella"

#~ msgid "Everyone can read"
#~ msgstr "Kaikki voivat lukea"

#~ msgid "Save"
#~ msgstr "Tallenna"

#~ msgid "Owner"
#~ msgstr "Omistaja"

#~ msgid "You can ask %s for access"
#~ msgstr "Voit pyytää käyttöoikeutta henkilöltä %s"

#~ msgid "The document was not updated"
#~ msgstr "Asiakirjaa ei päivitetty"

#~ msgid "Untitled Document"
#~ msgstr "Nimetön asiakirja"

#~ msgctxt "shortcut window"
#~ msgid "Rotate anti-clockwise"
#~ msgstr "Kierrä vastapäivään"

#~ msgid "LibreOffice is required to view this document"
#~ msgstr "Tämän asiakirjan katselu vaatii LibreOfficen"

#~ msgid "Category"
#~ msgstr "Luokka"

#~ msgid "Favorites"
#~ msgstr "Suosikit"

#~ msgid "Shared with you"
#~ msgstr "Kanssani jaetut"

#~ msgid "Online Accounts"
#~ msgstr "Verkkotilit"

#~| msgid "Documents"
#~ msgid "Documents folder"
#~ msgstr "Asiakirjat-kansio"

#~ msgid "You can add your online accounts in %s"
#~ msgstr "Voit lisätä verkkotilisi %s"

#~ msgid "Settings"
#~ msgstr "asetuksissa"

#~ msgid "Print"
#~ msgstr "Tulosta"

#~ msgid "Share"
#~ msgstr "Jaa"

#~| msgid "View items as a list"
#~ msgid "View items as a list or a grid"
#~ msgstr "Näytä kohteet luettelona tai ruudukkona"

#~ msgid "Recent"
#~ msgstr "Viimeisimmät"

#~ msgid ""
#~ "You don't have any collections yet. Enter a new collection name above."
#~ msgstr ""
#~ "Sinulla ei ole vielä kokoelmia. Anna uuden kokoelman nimi yläpuolelle."

#~ msgid "Create new collection"
#~ msgstr "Luo uusi kokoelma"

#~ msgid "Add to Collection"
#~ msgstr "Lisää kokoelmaan"

#~ msgid "Load More"
#~ msgstr "Lataa lisää"

#~| msgid ""
#~| "<li>View recent local and online documents</li> <li>Access your Google, "
#~| "ownCloud or SkyDrive content</li> <li>Search through documents</li> "
#~| "<li>See new documents shared by friends</li> <li>View documents "
#~| "fullscreen</li> <li>Print documents</li> <li>Select favorites</li> "
#~| "<li>Allow opening full featured editor for non-trivial changes</li>"
#~ msgid ""
#~ "<li>View recent local and online documents</li> <li>Access your Google, "
#~ "ownCloud or OneDrive content</li> <li>Search through documents</li> "
#~ "<li>See new documents shared by friends</li> <li>View documents "
#~ "fullscreen</li> <li>Print documents</li> <li>Select favorites</li> "
#~ "<li>Allow opening full featured editor for non-trivial changes</li>"
#~ msgstr ""
#~ "<li>Katsella paikallisia ja verkossa olevia asiakirjoja</li> <li>Käyttää "
#~ "Google-, ownCloud- ja OneDrive-tileillä olevaa sisältöä</li> <li>Etsiä "
#~ "asiakirjoista</li> <li>Nähdä tuttaviesi jakamat asiakirjat</li> "
#~ "<li>Katsella asiakirjoja koko näytön tilassa</li> <li>Tulostaa "
#~ "asiakirjoja</li> <li>Valita suosikkeja</li> <li>Avata suurempia muutoksia "
#~ "varten erillinen toimisto-ohjelmisto</li>"

#~ msgid "Results for “%s”"
#~ msgstr "Tulokset haulle \"%s\""

#~ msgid "Close"
#~ msgstr "Sulje"

#~ msgid "Skydrive"
#~ msgstr "Skydrive"

#~ msgid "Grid"
#~ msgstr "Ruudukko"

#~ msgid "List"
#~ msgstr "Luettelo"

#~ msgid "Organize"
#~ msgstr "Järjestä"

#~ msgid "Unable to load \"%s\" for preview"
#~ msgstr "Kohteen \"%s\" lataus esikatselua varten epäonnistui"

#~ msgid "Cannot find \"unoconv\", please check your LibreOffice installation"
#~ msgstr "Ohjelmaa \"unoconv\" ei löytynyt, tarkista LibreOffice-asennuksesi"

#~ msgid "filtered by title"
#~ msgstr "suodatettu otsikon perusteella"

#~ msgid "filtered by author"
#~ msgstr "suodatettu tekijän perusteella"

#~ msgid "Load %d more document"
#~ msgid_plural "Load %d more documents"
#~ msgstr[0] "Lataa %d asiakirja enemmän"
#~ msgstr[1] "Lataa %d asiakirjaa enemmän"

#~ msgid "Print..."
#~ msgstr "Tulosta..."

#~ msgid "Rotate Right"
#~ msgstr "Käännä oikealle"

#~ msgid "The active source filter"
#~ msgstr "Aktiivinen lähdesuodatin"

#~ msgid "The last active source filter"
#~ msgstr "Viimeisin aktiivinen lähdesuodatin"

#~ msgid "%d of %d"
#~ msgstr "%d/%d"

#~ msgid "GNOME Documents"
#~ msgstr "Gnome-asiakirjat"

#~ msgid "Remove from favorites"
#~ msgstr "Poista suosikeista"

#~ msgid "Add to favorites"
#~ msgstr "Lisää suosikkeihin"
